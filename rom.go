package main

func LoadRom(data []byte) {
	for b := 0; b < len(data); b+=2 {
		word := uint16(data[b]) << 8 | uint16(data[b+1])
		Ram[b / 2] = word
	}
}

