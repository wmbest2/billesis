package main

type Operand interface {
	Read() uint32
	ReadLong() uint32
	Write(uint32)
	WriteLong(uint32)
	Str() string
}

func GetRegOp(address *uint32, dis string) RegOp {
	var m RegOp
	m.address = address
	m.str = dis
	return m
}

type RegOp struct {
	address *uint32
	str string
}

func (o RegOp) Read() uint32 {
	return *o.address
}

func (o RegOp) ReadLong() uint32 {
	return *o.address
}

func (o RegOp) Write(val uint32) {
	*o.address = val
}

func (o RegOp) WriteLong(val uint32) {
	*o.address = val
}

func (o RegOp) Str() string {
	return o.str
}

func GetMemOp(address uint32, dis string) MemOp {
	var m MemOp
	m.address = address
	m.str = dis
	return m
}

type MemOp struct {
	address uint32
	str string
}

func (o MemOp) Read() uint32 {
	return uint32(Ram.Read(o.address))
}

func (o MemOp) ReadLong() uint32 {
	return uint32(Ram.Read(o.address)) << 16 | uint32(Ram.Read(o.address + 2))
}

func (o MemOp) Write(val uint32) {
	Ram[o.address] = uint16(val)
}

func (o MemOp) WriteLong(val uint32) {
	Ram[o.address] = uint16(val >> 16) 
	Ram[o.address+2] = uint16(val & 0xff)
}

func (o MemOp) Str() string {
	return o.str
}
