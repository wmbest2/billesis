package main

import (
	"fmt"
)

const (
	BYTE = iota
	WORD
	LONG
)

var (
	PC uint32
)

type Cpu struct {
	SSP uint32
	USP uint32
	SR  uint16
	D   [8]uint32
	A   [8]uint32
}

func (c *Cpu) Reset() {
	PC = 0x0
	for index, _ := range c.D {
		c.D[index] = 0x0
	}

	for index, _ := range c.A {
		c.A[index] = 0x0
	}
}

func (c *Cpu) SP() *uint32 {
	if c.IsSupervisor() {
		return &c.SSP
	}
	return &c.USP
}

func (c *Cpu) IsSupervisor() bool {
	return c.SR&0x2000 == 0x2000
}

func (c *Cpu) X() bool {
	return c.SR&0x10 == 0x10
}

func (c *Cpu) N() bool {
	return c.SR&0x8 == 0x8
}

func (c *Cpu) Z() bool {
	return c.SR&0x4 == 0x4
}

func (c *Cpu) V() bool {
	return c.SR&0x2 == 0x2
}

func (c *Cpu) C() bool {
	return c.SR&0x0 == 0x0
}

func (c *Cpu) EA(inst uint16, size int, second bool) Operand {
	reg := inst & 0x7
	mode := (inst >> 3) & 0x7
	if second {
		mode = (inst >> 6) & 0x7
		reg = (inst >> 9) & 0x7
	}

	sizeOffset := 2
	if size == WORD {
		sizeOffset = 4
	}

	switch mode {
	case 0x0: // D Reg
		return GetRegOp(&c.D[reg], fmt.Sprintf("D%d", reg))
	case 0x1: // A Reg
		return GetRegOp(&c.A[reg], fmt.Sprintf("A%d", reg))
	case 0x2: // Indirect A
		return GetMemOp(c.A[reg], fmt.Sprintf("(A%d)", reg))
	case 0x3: // Indirect A post Inc
		val := GetMemOp(c.A[reg], fmt.Sprintf("(A%d)+", reg))
		c.A[reg] += uint32(sizeOffset)
		return val
	case 0x4: // Indirect A pre dec
		c.A[reg] -= uint32(sizeOffset)
		return GetMemOp(c.A[reg], fmt.Sprintf("-(A%d)", reg))
	case 0x5:
		PC += 2
		offset := uint32(Ram.Read(PC) << 1)
		return GetMemOp(c.A[reg] + offset, fmt.Sprintf("(A%d) + %d", reg, offset))
	case 0x06:
		fmt.Printf("SIX")
		//return handleSix(inst);
	case 0x07:
		switch reg {
		case 0x0:
			fmt.Printf("7:0")
		case 0x1:
			address := Ram.ReadLong(PC + 2)
			PC += 4
			return GetMemOp(address, fmt.Sprintf("(0x%x).L", address))
		case 0x2:
			fmt.Printf("7:2")
		case 0x3:
			offset := Ram.Read(PC+2) << 1
			ret := GetMemOp(PC+uint32(offset), fmt.Sprintf("(PC + 0x%x)", offset))
			PC += 2
			return ret
		case 0x4:
			PC += 2
			return GetMemOp(PC, fmt.Sprintf("#%x", Ram.Read(PC)))
		}
	}
	return nil
}

func (c *Cpu) BitwiseImmediateGroup() {
	inst := Ram.Read(PC)
	switch inst >> 8 & 0xf {
	case 0x0:
		panic("OrI")
	case 0x2:
		c.AndI()
		return
	case 0x4:
		panic("SubI")
	case 0x6:
		panic("AddI")
	case 0xa:
		panic("EorI")
	case 0xc:
		panic("CmpI")
	}

	switch inst >> 6 & 0x07 {
	case 0x0:
		fallthrough
	case 0x4:
		c.Btst()
	case 0x1:
		panic("Bchg")
	case 0x2:
		panic("Bclr")
	case 0x3:
		panic("Bset")
	}
	panic("MoveP or Unsupported")
}

func (c *Cpu) QuickBranchGroup() {
	inst := Ram.Read(PC)
	switch inst >> 12 & 0xf {
	case 0x5:
		if inst&0x64 == 0x64 {
			panic("DBcc")
		} else if inst&0x60 == 0x60 {
			panic("Scc")
		} else if inst&0x10 == 0x10 {
			panic("AddQ")
		} else {
			panic("SubQ")
		}
	case 0x6:
		switch inst >> 8 & 0xf {
		case 0x0:
			panic("Bra")
		case 0x1:
			panic("Bsr")
		default:
			panic("Bcc")
		}
	case 0x7:
		panic("MoveQ")
	}
	panic("Unsupported")
}

func (c *Cpu) SystemGroup() {
	// System Instructions
	inst := Ram.Read(PC)
	switch inst {
	case 0x4AFC:
		panic("Illegal")
	case 0x4E70:
		panic("Reset")
	case 0x4E71:
		panic("Nop")
	case 0x4E72:
		panic("Stop")
	case 0x4E73:
		panic("Rte")
	case 0x4E75:
		panic("Rts")
	case 0x4E76:
		panic("TrapV")
	case 0x4E77:
		panic("Rtr")
	}

	switch inst >> 4 & 0xff {
	case 0xE4:
		panic("Trap")
	case 0xE5:
		panic("Link/Unlink")
	case 0xE6:
		panic("MOVE USP")
	}

	switch inst >> 8 & 0xf {
	case 0x0:
	case 0x4:
	case 0x6:
	case 0x8:
	case 0xa:
		panic("TST")
	case 0xc:
	case 0xe:
	}
	panic("LEA/CHK")
}

func (c *Cpu) Btst() {
	inst := Ram.Read(PC)
	t := inst >> 6
	if t == 32 {
		fmt.Printf("BTST.B\n")
	} else {
		fmt.Printf("BTST.L\n")
		PC += 2
	}
	PC += 2
}

func (c *Cpu) AndI() {
	inst := Ram.Read(PC)
	s := inst >> 6 & 0x3

	switch int(s) {
	case 0x0:
		/*value := Ram.Read(PC+2) & 0xff*/
		/*fmt.Printf("AndI $%#x, ", value)*/
		/*source := c.EA(inst, BYTE, false)*/
		/*c.SetValue(source, *source & &value)*/
		PC += 2
	case 0x1:
		/*value := Ram.Read(PC + 2)*/
		/*fmt.Printf("AndI $%#x, ", value)*/
		/*source := c.EA(inst, WORD, false)*/
		/*c.SetValue(source, *source & &value)*/
		PC += 2
	case 0x2:
		/*value := Ram.ReadLong(PC)*/
		/*fmt.Printf("AndI $%#x, ", value)*/
		/*source := c.EA(inst, LONG, false)*/
		/*c.SetValue(source, *source & &value)*/
		PC += 4
	}
	fmt.Printf("\n")
}

func (c *Cpu) Tst() {
}

func (c *Cpu) Move() {
	inst := Ram.Read(PC)
	op := inst >> 12

	var sizeStr string
	size := WORD
	switch int(op) {
	case 0x1:
		sizeStr = "B"
		size = BYTE
	case 0x2:
		sizeStr = "W"
		size = WORD
	case 0x3:
		sizeStr = "L"
		size = LONG
	}

	source := c.EA(inst, size, false)
	dest := c.EA(inst, size, true)
	if (size == LONG) {
		dest.WriteLong(source.ReadLong())
	} else {
		dest.Write(source.Read())
	}
	fmt.Printf("MOVE.%s %s, %s\n", sizeStr, source.Str(), dest.Str())
}

func (c *Cpu) Tick() bool {
	instruction := Ram.Read(PC)
	opcode := instruction >> 12

	fmt.Printf("Inst 0b%b\n", instruction)

	switch opcode {
	case 0x0:
		// Bitwise/Immediate Instructions
		c.BitwiseImmediateGroup()
	case 0x1:
		fallthrough
	case 0x2:
		fallthrough
	case 0x3:
		c.Move()
	case 0x4:
		c.SystemGroup()
	case 0x5:
		fallthrough
	case 0x6:
		fallthrough
	case 0x7:
		// Quick/Branch Instructions
		c.QuickBranchGroup()
	case 0x8:
		// Div/Or Instructions
		panic("Div / Or")
	case 0x9:
		// Sub instructions
		panic("Subtract")
	case 0xa:
		panic("Unknown")
	case 0xb:
		// Compare Instructions
		panic("Compare")
	case 0xc:
		// Mul/And Instructions
		panic("Mul / And")
	case 0xd:
		// Add Instructions
		panic("Add ")
	case 0xe:
		// Rotate/Shift Instructions
		panic("Rotate / Shift")
	}

	PC += 2
	fmt.Println("HEHRHREHER")
	return true
}
