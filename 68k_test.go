package main

import (
	"fmt"
	"testing"
)

func TestMoveDtoD(t *testing.T) {
	Ram.Init()
	cpu.Reset()
	cpu.D[0x1] = 0xffcc
	Ram[0x0] = 0x2601
	cpu.Tick()
	if cpu.D[0x3] != cpu.D[0x1] {
		t.Errorf("D3: 0x%x Expected: 0x%x", cpu.D[0x3], cpu.D[0x1])
	}
	fmt.Println("")
}

func TestMoveAtoA(t *testing.T) {
	Ram.Init()
	cpu.Reset()
	cpu.A[0x1] = 0xffcc
	Ram[0x0] = 0x3649
	cpu.Tick()
	if cpu.A[0x3] != cpu.A[0x1] {
		t.Errorf("A3: 0x%x Expected: 0x%x", cpu.A[0x3], cpu.A[0x1])
	}
	fmt.Println("")
}

func TestMoveDtoA(t *testing.T) {
	Ram.Init()
	cpu.Reset()
	cpu.D[0x1] = 0xffcc
	Ram[0x0] = 0x3641
	cpu.Tick()
	if cpu.A[0x3] != cpu.D[0x1] {
		t.Errorf("A3: 0x%x Expected: 0x%x", cpu.A[0x3], cpu.D[0x1])
	}
	fmt.Println("")
}

func TestMoveAtoD(t *testing.T) {
	fmt.Println("===========Test Move A to D==============")
	Ram.Init()
	cpu.Reset()
	cpu.A[0x1] = 0xffcc
	Ram[0x0] = 0x3609
	cpu.Tick()
	if cpu.D[0x3] != cpu.A[0x1] {
		t.Errorf("A3: 0x%x Expected: 0x%x", cpu.D[0x3], cpu.A[0x1])
	}
	fmt.Println("")
}

func TestMoveIndirectPost(t *testing.T) {
	fmt.Println("===========Test Move Indirect Post==============")
	Ram.Init()
	cpu.Reset()
	cpu.A[0x1] = 0xffcc
	Ram[0x0] = 0x387C
	Ram[0x1] = 0x100
	Ram[0x2] = 0x38C9
	cpu.Tick()
	cpu.Tick()
	if cpu.A[0x4] != 0x102 {
		t.Errorf("A4: 0x%x Expected: 0x%x", cpu.A[0x4], 0x102)
	}

	if Ram.Read(0x100) != 0xffcc {
		t.Errorf("Ram[0x100]: 0x%x Expected: 0x%x", Ram.Read(0x100), 0xffcc)
	}
	fmt.Println("")
}

func TestMoveIndirectPre(t *testing.T) {
	fmt.Println("===========Test Move Indirect Pre==============")
	Ram.Init()
	cpu.Reset()
	cpu.A[0x1] = 0xffcc
	cpu.A[0x4] = 0x100
	Ram[0x0] = 0x3909
	cpu.Tick()
	if cpu.A[0x4] != 0x0fe {
		t.Errorf("A4: 0x%x Expected: 0x%x", cpu.A[0x4], 0x0fe)
	}

	if Ram.Read(0x0fe) != 0xffcc {
		t.Errorf("Ram[0x100]: 0x%x Expected: 0x%x", Ram.Read(0x0fe), 0xffcc)
	}
	fmt.Println("")
}

func TestMoveIndirectOffset(t *testing.T) {
	fmt.Println("===========Test Move Indirect Offset==============")
	Ram.Init()
	cpu.Reset()

	cpu.D[0x1] = 0xffcc
	cpu.A[0x1] = 0x100
	cpu.A[0x2] = 0x200

	Ram[0x0] = 0x3341
	Ram[0x1] = 0x0003
	Ram[0x2] = 0x3569
	Ram[0x3] = 0x0003
	Ram[0x4] = 0x0002

	cpu.Tick()
	cpu.Tick()

	if Ram.Read(0x106) != 0xffcc {
		t.Errorf("Ram[0x10e]: 0x%x Expected: 0x%x", Ram.Read(0x0106), 0xffcc)
	}

	if Ram.Read(0x204) != 0xffcc {
		t.Errorf("Ram[0x100]: 0x%x Expected: 0x%x", Ram.Read(0x0202), 0xffcc)
	}
	fmt.Println("")
}
