package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

var (
	cpu Cpu
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please specify a ROM file")
		return
	}

	if contents, err := ioutil.ReadFile(os.Args[len(os.Args)-1]); err == nil {
		LoadRom(contents)
		*cpu.SP() = Ram.ReadLong(0)
		PC = Ram.ReadLong(4)
		for {
			cpu.Tick() 
			fmt.Printf("=======\nSP: %#x\nPC: %#x\nD:%x\nA:%x\n", *cpu.SP(), PC, cpu.D, cpu.A)
			time.Sleep(3000 * time.Millisecond)
		}
	} else {
		fmt.Println(err.Error())
		return
	}
}
