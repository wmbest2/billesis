package main

type Memory [0xffffff]uint16

var (
	Ram Memory 
)

func (m *Memory) Init() {
	for index, _ := range m {
		m[index] = 0x0
	}
}

func (m *Memory) Read(index uint32) uint16 {
	return Ram[index >> 1]
}

func (m *Memory) ReadLong(index uint32) uint32 {
	return uint32(Ram[index >> 1]) << 16 | uint32(Ram[index >> 1 + 1])
}

func (m *Memory) Address(index uint32) *uint16 {
	return &Ram[index >> 1]
}
